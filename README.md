# pandoc with PlantUML in a container

This project provides a container image definition (read: Docker, Podman image)
for rendering of documents from different formats to various formats, using:

- [Pandoc v2.18](https://pandoc.org);
- [PlantUML v1.2022.4](https://plantuml.com), using Java Openjdk 18.0.1;
- [pandoc-plantuml-filter](https://github.com/timofurrer/pandoc-plantuml-filter).

This image is published on Docker Hub, as [ojob/pandoc-plantuml](https://hub.docker.com/repository/docker/ojob/pandoc-plantuml).

This is forked from previous work, simply updating to current software versions:

- [`docker-pandoc-with-plantuml` by koduki](https://github.com/koduki/docker-pandoc-with-plantuml)
- [`docker-pandoc-with-plantuml` by Nicolas PERNOUD](https://forge.grandlyon.com/NPERNOUD/docker-pandoc-with-plantuml)

## Usage

This image can be used in a document-rendering-build-script:

```bash
#!/usr/bin/bash
sudo podman run \
	-v `pwd`:/var/docs/:ro \
	-v `pwd`/plantuml-images/:/var/docs/plantuml-images/:rw \
	-v `pwd`/public/:/var/docs/public/:rw \
	ojob/pandoc-plantuml \
	--presentation.yml \
	$*
```

And then in a CI pipeline, for instance in Gitlab:

```yaml
.pages-build:
  image: ojob/pandoc-plantuml
  entrypoint: [""]
  script:
    - pandoc --version
    # create output folder for the rendered document, and PlantUML temp folder
    - mkdir public/
    - mkdir plantuml-images/
    # convert the document
    - pandoc --filter pandoc-plantuml --defaults presentation.yml
  artifacts:
    paths:
      - public
```

